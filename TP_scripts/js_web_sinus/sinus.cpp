#include <emscripten/bind.h>
#include <cmath>

struct Color {
    uint8_t _r;
    uint8_t _g;
    uint8_t _b;
    uint8_t _a;
};

class Sinus {

    private:
        int _width;
        int _height;
        std::vector<Color> _data;

    public:
        Sinus(int width, int height) : 
            _width(width), _height(height), _data(width*height) {
        }

        emscripten::val update(float freq,float phase) { 
            
            for( int x=0; x<_width;x++){
                for( int y=0; y<_height;y++){
                    _data[y*_width+x] = { (uint8_t)(sin(2*M_PI*(freq*x)/_width+phase)*255) , 0, 0, 255};
                }
            }

            /*for (Color & c : _data){
                    c = { (uint8_t)(sin(M_PI*i*j/(_width))*255*freq) , 0, 0, 255};
                    i++;
                    if(i==_width) j++;
            }*/
                

            size_t s = _data.size()*4;
            uint8_t * d = (uint8_t *) _data.data();
            return emscripten::val(emscripten::typed_memory_view(s, d));
        }
};

EMSCRIPTEN_BINDINGS(sinus) {
    emscripten::class_<Sinus>("Sinus")
        .constructor<int, int>()
        .function("update", &Sinus::update);
}

