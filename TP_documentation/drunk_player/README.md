# Drunk_player

## Description

Drunk_player est un système de lecture de vidéos qui a trop bu.

Drunk_player est composé :
- d'une bibliotèques
- d'un programme graphique
- d'un programme console


## Dépendances

- OpenCV
- Boost

## Compilation

```
mkdir build
cb build
cmake ..
make
```

## Utilisation

```
./dunk_player_gui.out ../data/
```

![Exemple](drunk_player_gui.png)